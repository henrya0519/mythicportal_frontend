import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthComponent } from './components/auth/auth.component';
import { AuthGuard } from './guards/auth-guard';


const routes: Routes = [
  {
    path: "auth",
    component: AuthComponent
  },
  {
    path: "main",
    /**
     * Importacióndel modulo
     */
    loadChildren: () =>
      import("./modules/main/main.module").then(
        m => m.MainModule
      ),
    canActivate: [AuthGuard]
  },
  { path: "**", pathMatch: "full", redirectTo: "/" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
