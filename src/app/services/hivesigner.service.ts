import { Injectable } from '@angular/core';
import { Client } from 'hivesigner'

@Injectable({
  providedIn: 'root'
})
export class HiveSignerService {

  public clientHiveSigner: Client

  constructor() {
    this.clientHiveSigner = new Client({
      app: 'demo',
      callbackURL: 'http://localhost:4200/#/auth'
    });
  }

}
