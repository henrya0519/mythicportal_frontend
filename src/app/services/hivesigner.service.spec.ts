import { TestBed } from '@angular/core/testing';

import { HivesignerService } from './hivesigner.service';

describe('HivesignerService', () => {
  let service: HivesignerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HivesignerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
