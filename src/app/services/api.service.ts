import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private apiUrl: string = null;

  constructor(private httpClient: HttpClient) {

  }

  public loginWithHive(): Observable<any> {
    return this.httpClient.get(environment.host + "login");
  }

}
