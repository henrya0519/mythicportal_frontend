import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HiveSignerService } from 'src/app/services/hivesigner.service';
import { RedirectService } from 'src/app/services/redirect.service';
import { LoginComponent } from '../login/login.component';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  constructor(private hiveSignerService: HiveSignerService, private route: ActivatedRoute, private redirectService: RedirectService) { }

  ngOnInit(): void {
    if (!localStorage.getItem('accountToken')) {
      this.route.queryParams.subscribe(
        params => {
          let accessToken = params['access_token']
          if (accessToken) {
            localStorage.setItem('accountToken', accessToken)
            this.hiveSignerService.clientHiveSigner.setAccessToken(accessToken)
            this.getUserInfo()
          } else {
            LoginComponent.showLoginModal()
          }
        }
      )
    }
  }

  getUserInfo() {
    this.hiveSignerService.clientHiveSigner.me()
      .then((data) => {
        localStorage.setItem('userData', JSON.stringify(data))
        this.redirectService.navigate('main/home')
      }).catch(error => {
        console.log(error)
      })
  }
}
