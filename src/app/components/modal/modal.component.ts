import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  constructor() { }

  ngOnInit(): void { }

  static showLoadModal(tittle?: string, message?: string) {
    $("#titleLoadModal").html(tittle || 'Loading');
    $("#messageLoadModal").html(message || '');
    $("#loadModal").modal({ backdrop: "static", keyboard: false });
  }

  static closeLoadModal() {
    setTimeout(() => {
      $("#loadModal").modal("hide");
    }, 500);
  }

  static showInfoModal(tittle?: string, message?: string) {
    $("#titleInfoModal").html(tittle || 'Info');
    $("#messageInfoModal").html(message || '');
    $("#infoModal").modal();
  }

  static closeInfoModal() {
    setTimeout(() => {
      $("#infoModal").modal("hide");
    }, 500);
  }
}
