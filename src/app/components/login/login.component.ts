import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { HiveSignerService } from 'src/app/services/hivesigner.service';
import { ModalComponent } from '../modal/modal.component';

declare var $: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private apiService: ApiService, private hiveSignerService: HiveSignerService) { }

  ngOnInit(): void { }

  static showLoginModal() {
    $("#loginModal").modal({ backdrop: "static", keyboard: false });
  }

  loginWithHive() {
    this.hiveSignerService.clientHiveSigner.login({ state: 'asdfg' })
  }

}
