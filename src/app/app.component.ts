import { Component, OnInit } from '@angular/core';
import { LoginComponent } from './components/login/login.component';
import { RedirectService } from './services/redirect.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'mythicportal';

  constructor(private redirectService: RedirectService) { }

  ngOnInit(): void {
    setTimeout(() => {
      if (localStorage.getItem('userData')) {
        this.redirectService.navigate('main/home')
      } else {
        LoginComponent.showLoginModal()
      }
    }, 2000)
  }
}
