import { Injectable } from "@angular/core";
import { CanActivate, CanActivateChild, Router, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from "@angular/router";
import { Observable } from "rxjs";

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanActivateChild {

    constructor(private router: Router) { }

    /**
     * Validacion de sesion activa
     */
    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        return this.validate()
    }

    /**
     * 
     * @param childRoute 
     * @param state 
     */
    canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
        return this.validate()
    }

    /**
     * Funcion para validar la sesion activa del usuario
     */
    private validate(): boolean {
        if (!localStorage.getItem('accountToken')) {
            this.secureSite()
            return false
        }
        return true
    }

    private secureSite() {
        console.log('Redirect secure site')
        this.router.navigate(['/'])
    }

}
